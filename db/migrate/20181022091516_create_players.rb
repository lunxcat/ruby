class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :nickname
      t.string :rank
      t.string :charisma
      t.string :wisdow
      t.string :int

      t.timestamps
    end
  end
end
